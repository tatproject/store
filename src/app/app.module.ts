import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NewCustomerComponent } from './components/new-customer/new-customer.component';
import { AppRoutingModule } from './app-routing.module';
import {ButtonModule} from 'primeng-lts/button'
import {PasswordModule } from 'primeng-lts/password';
import { ConfirmationService } from 'primeng/api';
import {DynamicDialogModule} from 'primeng-lts/dynamicdialog';
import {InputNumberModule} from 'primeng/inputnumber';
import { DebtsComponent } from './components/debts/debts.component';
import {ChartModule} from 'primeng/chart';
import { HttpClientModule } from '@angular/common/http';
import {TableModule} from 'primeng/table';
import { DialogService} from 'primeng-lts/dynamicdialog'
import { NewSupplierComponent } from './components/new-supplier/new-supplier.component';
import { StudentsListComponent } from './components/students-list/students-list.component';
import { FormsModule } from '@angular/forms';
import { StoreComponent } from './components/store/store.component';
import { SuppliersListComponent } from './components/‏‏suppliers-list/suppliers-list.component';

import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import {SlideMenuModule} from 'primeng/slidemenu';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CategoryComponent } from './components/category/category.component';
import { HaederComponent } from './components/haeder/haeder.component';
import { NewProductPopUpComponent } from './components/popUps/new-product-pop-up/new-product-pop-up.component';
import { BasketComponent } from './components/basket/basket.component';
import { DebtsPopUpComponent } from './components/popUps/debts-pop-up/debts-pop-up.component';
import { DeletePopUpComponent } from './components/popUps/delete-pop-up/delete-pop-up.component';




@NgModule({
  declarations: [
    NavBarComponent,
    AppComponent,
    HomeComponent,
    LoginComponent,
    NewCustomerComponent,
    DebtsComponent,
    NewSupplierComponent,
    StudentsListComponent,
    SuppliersListComponent,
    StoreComponent,
    CategoryComponent,
    HaederComponent,
    NewProductPopUpComponent,
    BasketComponent,
    DebtsPopUpComponent,
    DeletePopUpComponent

    
    
    
  ],
  imports: [
    BrowserAnimationsModule,
    SlideMenuModule,
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    ButtonModule,
    PasswordModule,
    DynamicDialogModule,
        InputNumberModule,
    ChartModule,
    TableModule,FormsModule,HttpClientModule
  

  
     
  ],
  providers: [DialogService],
  bootstrap: [AppComponent]
})
export class AppModule { }

