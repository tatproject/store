import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
items: MenuItem[]=[]
 constructor( ) {}
 ngOnInit() {

    this.items = [
        {
            label: 'דף הבית',
            icon: 'pi pi-fw pi-home', 
            routerLink: ['/home']
                
        },
        {
            label: 'תלמידים',
            icon: 'pi pi-fw pi-users',
            routerLink: ['/students-list']
        },
        {
            label: 'ספקים',
            icon: 'pi pi-fw pi-briefcase',
            routerLink: ['/suppliers-list']
        },
        {
            label: 'מלאי',
            icon: 'pi pi-fw pi-table',
            routerLink: ['/categorys']
        },
        {
            label: 'קניות',
            icon: 'pi pi-fw pi-shopping-cart',
            routerLink: ['/basket']
        }
    ];
}

   

}

