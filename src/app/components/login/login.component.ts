import { LoginService } from 'src/app/services/login.service';
import {LoginModel} from '../../Models/LoginModel'
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginModel :LoginModel = new LoginModel() ;
  password:string = "";
  user = "";
  constructor(private _LoginService:LoginService, private router:Router) { }

  ngOnInit(): void {
  }
  login(){

 this.loginModel.Password = this.password;
 this.loginModel.UserName = this.user;
 this._LoginService.login(this.loginModel).subscribe(res=>{

   if(res.ResponseCode == "101")
   {
     alert("user name or password is incorrect");
   }
   else{
     this.router.navigate(['/home']);
     
   }
 });

  }
  
}
