import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DynamicDialogRef } from 'primeng-lts/dynamicdialog';
import { DialogService } from 'primeng-lts/dynamicdialog';
import { Product, Quantity } from 'src/app/Models/products';
import { BasketService } from 'src/app/services/basket.service';
import { GetProductsService } from 'src/app/services/get-products.service';
import { NewProductPopUpComponent } from '../popUps/new-product-pop-up/new-product-pop-up.component';
@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css'],
})
export class StoreComponent implements OnInit {
  constructor(
    private GetproductsService: GetProductsService,
    private ActivatrRoute: ActivatedRoute,
    public dialogService: DialogService,
    private basketService: BasketService,
    private router: Router
  ) {}
  products: Product[] = [];

  productse = true;
  quantities: Quantity[]= [];
  imageUrl = '../../assets/img/storeItems/shirts.jpg';
  ref: DynamicDialogRef | undefined;
  id = '0';
  ngOnInit(): void {
    if (this.ActivatrRoute.snapshot.paramMap.get('id') != null) {
      this.id = this.ActivatrRoute.snapshot.paramMap.get('id')!;
      switch (this.id) {
        case '2':
          this.imageUrl = '../../assets/img/storeItems/pants33.jpg';
          break;
        case '3':
          this.imageUrl = '../../assets/img/storeItems/under.jpg';
          break;
        case '4':
          this.imageUrl = '../../assets/img/storeItems/diord.jpg';
          break;
        case '5':
          this.imageUrl = '../../assets/img/storeItems/shirts.jpg';
          break;
        case '11':
          this.imageUrl = '../../assets/img/storeItems/sacks2.jpg';
          break;
        case '13':
          this.imageUrl = '../../assets/img/storeItems/sueder.jpg';
          break;
        default:
          break;
      }
    }
    this.getstoreitems();
  }
  getstoreitems() {
    this.GetproductsService.getStoreItems(this.id).subscribe((res) => {
      console.log(res);
      this.products = res.storeItemsModels;
      console.log(this.products.length, ' this.products');
      if (this.products.length == 0) {
        this.productse = false;
      }
    });
  }
  buy() {}

  editProduct(Product: Product) {
    console.log(Product, 'Product');
    this.ref = this.dialogService.open(NewProductPopUpComponent, {
      data: {
        product: Product,
      },
      header: 'ערוך פריט',
      width: '60%',
    });
  }
  DeleteProduct(Product: Product) {
    this.GetproductsService.DeleteProduct(Product.id).subscribe((res) => {
      console.log(res);
      alert('הפריט הוסר בהצלחה');
    });
  }
  AddToBasket(Product: Product) {
    this.basketService.AddToBasket(Product.id , Product.quantityForBasket.toString());
  }
  GoToBasket() {
    this.router.navigate(['/basket']);
  }
  Back() {
    this.router.navigate(['/categorys']);
  }
 /*  removeItem(product: Product) {
    let index = this.products.indexOf(product)
   if(index != undefined && this.products[index].quantityForBasket > 0)
   {
    this.products[index].quantityForBasket -=1;
   }
    
  }
  addItem(product: Product) {
    let index = this.products.indexOf(product)
    console.log(index , "index" , this.products[index])
    if(index != undefined && +this.products[index].quantity! > this.products[index].quantityForBasket )
    {
     this.products[index].quantityForBasket +=1;
    }
    
    
  } */

}
