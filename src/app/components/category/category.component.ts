import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { groupBy } from 'rxjs/operators';
import { Categorys, Product } from 'src/app/Models/products';
import { GetProductsService } from 'src/app/services/get-products.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  constructor(private GetproductsService:GetProductsService , private router: Router , private ActivatrRoute: ActivatedRoute) { }
  products: Product[] =[];
  cat : Categorys[] = [];

  ngOnInit(): void {
  
    this.getstoreitems();
  }
  getstoreitems(){
    this.GetproductsService.getCategorys().subscribe(res => { 
      console.log(res , "ressss");
      this.cat = res

     // this.products=res.storeItemsModels;
    })
}
GetItems(category : Categorys)
{
  const id = category.categoryId
  this.router.navigate(['/store/', id] )

}
getImage(id : string){
  let image = "../../../assets/img/icons/"
  let mg = "";
  switch (id) {
    case "4":
      mg = "diordicon"
      break;
      case "2":
      mg = "panicon"
      break;
      case "5":
      mg = "shicon"
      break;
      case "11":
      mg = "socksicon"
      break;
      case "3":
        mg = "undershirticon"
        break;
  
    default:
      mg = "dificon"
      break;

  }
  return image+mg+".jpg"
}
}
