import { Component, OnInit } from '@angular/core';
import {DynamicDialogRef} from 'primeng-lts/dynamicdialog';
import {DynamicDialogConfig} from 'primeng-lts/dynamicdialog'
import { Debts } from 'src/app/Models/DebtsModel';
import { BasketService } from 'src/app/services/basket.service';
import { GetStudentsService } from 'src/app/services/‏‏get-students.service';
@Component({
  selector: 'app-delete-pop-up',
  templateUrl: './delete-pop-up.component.html',
  styleUrls: ['./delete-pop-up.component.css']
})
export class DeletePopUpComponent implements OnInit {


  constructor(public ref: DynamicDialogRef , public config: DynamicDialogConfig , public StudentsService : GetStudentsService , private basketService:BasketService) { }

  ngOnInit(): void {

  }
  cancel(){
   this.ref.close(true)
  }
  save(){
    this.ref.close(false)
  }
  
}
