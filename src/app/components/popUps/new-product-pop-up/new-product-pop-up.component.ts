import { Component, OnInit } from '@angular/core';
//import { DynamicDialogConfig, DynamicDialogRef } from 'primeng-lts/dynamicdialog';
import {DynamicDialogRef} from 'primeng-lts/dynamicdialog';
import {DynamicDialogConfig} from 'primeng-lts/dynamicdialog'
import { Product } from 'src/app/Models/products';
import { GetProductsService } from 'src/app/services/get-products.service';

@Component({
  selector: 'app-new-product-pop-up',
  templateUrl: './new-product-pop-up.component.html',
  styleUrls: ['./new-product-pop-up.component.css']
})
export class NewProductPopUpComponent implements OnInit {
  products: Product = new Product();
  description :string = "";
  color :string = "";
  price :string = "";
  compeny :string = "";
  size :string = "";
  category :string = "";
  quantity :string = "";
  code :string = "";
  image :string ="";
  id : string = ""
  SupplierId : string ="";
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig , private ProductsService: GetProductsService) { }

  ngOnInit(): void {
   console.log(this.config.data.product , "fkvbdfhvg")
   if(this.config.data.product == null)
   {
     
   }
   else{
   this.color = this.config.data.product.color;
   this.size = this.config.data.product.size;
   this.code = this.config.data.product.code;
   this.compeny = this.config.data.product.compeny;
   this.description = this.config.data.product.description;
   this.image = this.config.data.product.image;
   this.quantity = this.config.data.product.quantity;
   this.price = this.config.data.product.price;
   this.category = this.config.data.product.category;
   this.SupplierId = this.config.data.product.SupplierId
   }
  }
  editProduct()
  {
    this.products.Code = this.code;
    this.products.size = this.size;
    this.products.color = this.color;
    this.products.Compeny = this.compeny;
    this.products.description = this.description;
    this.products.image = this.image;
    this.products.quantity = this.quantity;
    this.products.price = this.price;
    this.products.category = this.category;
    this.products.id =this.config.data.product.id;
    console.log( this.products.id , "this.ID")
    this.ProductsService.UpdateProduct(this.products).subscribe(res=>{
      if(res == true)
      {
        alert("sucsess")
        this.ref.close();
        window.location.reload();
      }
      else{
        alert("faild")
        this.ref.close();
        window.location.reload();
      }
    })
  }
  addNewProduct()
  {
    this.products.Code = this.code;
    this.products.size = this.size;
    this.products.color = this.color;
    this.products.Compeny = this.compeny;
    this.products.description = this.description;
    this.products.image = this.image;
    this.products.quantity = this.quantity;
    this.products.price = this.price;
    this.products.category = this.category;
    this.ProductsService.NewProduct(this.products).subscribe(res=>{
      if(res == true)
      {
        alert("sucsess")
        this.ref.close();
        window.location.reload();
      }
      else{
        alert("faild")
        this.ref.close();
       
      }
    })
  }

}
