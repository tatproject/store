import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewProductPopUpComponent } from './new-product-pop-up.component';

describe('NewProductPopUpComponent', () => {
  let component: NewProductPopUpComponent;
  let fixture: ComponentFixture<NewProductPopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewProductPopUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewProductPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
