import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DebtsPopUpComponent } from './debts-pop-up.component';

describe('DebtsPopUpComponent', () => {
  let component: DebtsPopUpComponent;
  let fixture: ComponentFixture<DebtsPopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DebtsPopUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DebtsPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
