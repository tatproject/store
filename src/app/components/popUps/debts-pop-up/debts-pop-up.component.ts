import { Component, OnInit } from '@angular/core';
import {DynamicDialogRef} from 'primeng-lts/dynamicdialog';
import {DynamicDialogConfig} from 'primeng-lts/dynamicdialog'
import { Debts } from 'src/app/Models/DebtsModel';
import { BasketService } from 'src/app/services/basket.service';
import { GetStudentsService } from 'src/app/services/‏‏get-students.service';
@Component({
  selector: 'app-debts-pop-up',
  templateUrl: './debts-pop-up.component.html',
  styleUrls: ['./debts-pop-up.component.css']
})
export class DebtsPopUpComponent implements OnInit {
name = "";
sum = '0'
Debts :Debts = new Debts();
myDate = new Date();
  constructor(public ref: DynamicDialogRef , public config: DynamicDialogConfig , public StudentsService : GetStudentsService , private basketService:BasketService) { }

  ngOnInit(): void {
    this.name = this.config.data.Student.FirstName + " " + this.config.data.Student.LastName
    console.log(this.config.data.Student , "bhjb")
    this.sum = sessionStorage.getItem('sum')!;
  }
  cancel(){
   this.ref.close();
  }
  save(){
   this.Debts.debtAmount = this.sum;
 //  this.Debts.date= this.myDate.toString();
   this.Debts.studentId = this.config.data.Student.CustomerId;
    this.StudentsService.AddDebtsToStudend( this.Debts).subscribe(res=>{
      this.basketService.ClearBasket();
      sessionStorage.setItem('sum' , "0")
      this.ref.close();
    })
  }
}
