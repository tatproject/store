import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-haeder',
  templateUrl: './haeder.component.html',
  styleUrls: ['./haeder.component.css']
})
export class HaederComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  route(id:number){
    if(id ==1)
    this.router.navigate(['/debts']);
   else if(id ==2)
    this.router.navigate(['/new-customer']);
    else if(id ==3)
    this.router.navigate(['/login']);
}
}
