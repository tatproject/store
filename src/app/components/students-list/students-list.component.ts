import { Component, OnInit } from '@angular/core';
import { Students } from 'src/app/Models/students';
import { GetStudentsService } from 'src/app/services/‏‏get-students.service';
import { Router } from '@angular/router';
import { NewCustomerComponent } from '../new-customer/new-customer.component';
import { Debts } from 'src/app/Models/DebtsModel';
import { NewProductPopUpComponent } from '../popUps/new-product-pop-up/new-product-pop-up.component';
import { DynamicDialogRef } from 'primeng-lts/dynamicdialog';
import { DialogService} from 'primeng-lts/dynamicdialog'
import { DebtsPopUpComponent } from '../popUps/debts-pop-up/debts-pop-up.component';
import { DebtsComponent } from '../debts/debts.component';
import { DeletePopUpComponent } from '../popUps/delete-pop-up/delete-pop-up.component';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.css']
})
export class StudentsListComponent implements OnInit {
  searchWord="";
  firstName:string = "";
 student: Students = new Students();
constructor(private GetstudentsService:GetStudentsService,private router:Router , public dialogService: DialogService) { 
  }
   ref: DynamicDialogRef ;
   filteredStudents: Students[] =[];
  students: Students[] =[];
  debts:Debts[]=[];
  first = 0;
  sum ="";

  rows = 10;

  ngOnInit():void {
    this.getstudentlist();
  }
  route(id:number){
    if(id ==1)
    this.router.navigate(['/debts']);
   else if(id ==2)
   this.editStudent()
   //NewCustomerComponent
    //this.router.navigate(['/new-customer']);
    else if(id ==3)
    this.router.navigate(['/login']);
}

editStudent()
    {
         this.ref = this.dialogService.open(NewCustomerComponent , {
       data:{
        // product : Product
       },
       header : 'תלמיד חדש',
       width : '50%'
     })
    }



     // this.GetStudentsService.getStudentList().then(students => this.students = students);
  

  next() {
      this.first = this.first + this.rows;
  }
  prev() {
      this.first = this.first - this.rows;
  }
  reset() {
      this.first = 0;
  }
  isLastPage(): boolean {
      return this.students ? this.first === (this.students.length - this.rows): true;
  }
  isFirstPage(): boolean {
      return this.students ? this.first === 0 : true;
  }
  
  getstudentlist(){
    this.GetstudentsService.getStudentList().subscribe(res => { 
      console.log(res);
      this.students=res.studentListModel;
    })

  }
  
  search(){
    console.log(this.searchWord)
         if(!this.searchWord){this.getstudentlist()}
        else{this.students.forEach(item=>{item.FirstName?.indexOf(this.searchWord)!== -1?
          this.filteredStudents.push(item):
        console.log("jj")})
        this.students=this.filteredStudents
        this.filteredStudents=[]
      }
   }

  deletestudent(CustomerId:any){
    const dialogRef = this.dialogService.open(DeletePopUpComponent , {
      header : 'שים לב!',
      width : '50%'
    });
    dialogRef.onClose.subscribe(res=>{
      if(res == false)
      {  this.GetstudentsService.deleteStudent(CustomerId).subscribe(res => { 
        this.router.navigate(['/students-list']).then(() => {
          window.location.reload();});
     })

      }
      console.log(res , "response from ref")
    })

  }
    
    UpdateCustomer(Customer:Students){
   
      this.ref = this.dialogService.open(NewCustomerComponent , {
        data:{
          Customer : Customer
        },
        header : 'ערוך תלמיד',
        width : '50%'
      }); 
      }
    getDebts(CustomerId:string){
      this.router.navigate(['/debts/' , CustomerId]);
    /*   this.GetstudentsService.GetDebts(CustomerId).subscribe(res => { 
        console.log(res);
        this.debts=res.debtsListModel;
        console.log(this.debts);
    }); */
     }
   AddDebets(Student:Students){
    this.sum = sessionStorage.getItem('sum')!;
    if(this.sum == "0")
    {

    }
    else{

    
    this.ref = this.dialogService.open(DebtsPopUpComponent , {
      data:{
        Student : Student
      },
      header : 'שים לב!',
      width : '50%'
    }); }
     
        }
        // search(){
        //     this.GetstudentsService.Search(this.firstName).subscribe(res => { 
        //       this.students=res;
        //       console.log(res);
        //     })
              
        
        //     }
 }
    


