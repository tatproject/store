import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  data:any;
    
    options: any;
  constructor(private router:Router) { 
    

    this.data = {
      
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [
          {   backgroundColor: '#C6A568',
              label: 'שנה קודמת',
              data: [65, 59, 80, 81, 56, 55, 40]
              
            
              
          },
          {   backgroundColor: '#0D5E7C',
              
              label: 'שנה נוכחית',
              data: [28, 48, 40, 19, 86, 27, 90]
          }
      ]
  }
  
  this.options = {
      title: {
          display: true,
          text: 'My Title',
          fontSize: 16
        
    
          
      },
      legend: {
          position: 'bottom'
      }
  };
  }

  ngOnInit(): void {
  }
  route(id:number){
if(id ==1){
  this.router.navigate(['/students-list']);
}
 else if(id ==2){
  this.router.navigate(['/suppliers-list']);
} 
 else if(id ==3){
  this.router.navigate(['/categorys']);
} 
else if(id ==4){
  this.router.navigate(['/debts']);
}
 
  }signout(){
  this.router.navigate(['/login']);
}
}


