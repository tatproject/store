import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/Models/products';
import { BasketService } from 'src/app/services/basket.service';
import { GetProductsService } from 'src/app/services/get-products.service';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {

  constructor(private router: Router , private GetproductsService:GetProductsService , private basketService:BasketService ) { }
  products: Product[] =[];
  sum = 0;
  productse = true;
  ngOnInit(): void {
    this.getstoreitems()
  }
  Back(){
    this.router.navigate(['/categorys'] )
  }
  getstoreitems(){
   let basketItems =  sessionStorage.getItem('Basket')
    console.log(basketItems ,"basketItems")
    this.GetproductsService.getBasketItems(basketItems!).subscribe(res => { 
      this.products=res.storeItemsModels;
      this.products.forEach(element => {
      this.sum += Number(element.price);
      });
      console.log( this.products.length , " this.products")
      if(this.products.length  == 0){
    this.productse = false;
      }
    })
}
DeleteProduct(products: Product){


}

DeleteAllProducts(){
  sessionStorage.setItem('Basket' , '');
}
end(){
let basketItems =  sessionStorage.getItem('Basket')
    //RemoveBasketItems
    this.GetproductsService.RemoveBasketItems(basketItems!).subscribe(res => { 
      console.log(res);
      this.basketService.ClearBasket();
    });
     

}

AddToDebts(){
console.log(this.sum , "this.sum")
sessionStorage.setItem('sum' , this.sum.toString());
this.router.navigate(['/students-list'])
}
removeItem(product: Product) {
  let index = this.products.indexOf(product)
 if(index != undefined && this.products[index].quantityForBasket > 0)
 {
  this.sum -= +this.products[index].price;
  this.products[index].quantityForBasket -=1;
 }
  
}
addItem(product: Product) {
  let index = this.products.indexOf(product)
  console.log(index , "index" , this.products[index])
  if(index != undefined && +this.products[index].quantity! > this.products[index].quantityForBasket )
  {
    console.log("basketq" , this.products)
    this.sum += +this.products[index].price;
   this.products[index].quantityForBasket +=1;
  }
  
  
}
}
