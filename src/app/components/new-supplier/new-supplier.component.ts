import { Component, OnInit } from '@angular/core';
import { Suppliers } from 'src/app/Models/suppliers';
import { GetSuppliersService } from 'src/app/services/‏‏‏‏get-suppliers.service';
import { Router } from '@angular/router';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng-lts/dynamicdialog';
@Component({
  selector: 'app-new-supplier',
  templateUrl: './new-supplier.component.html',
  styleUrls: ['./new-supplier.component.css']
})
export class NewSupplierComponent implements OnInit {
  supplierName="";
  zone:string = "";
  address = "";
  telephone=" ";
  city="";
  contactName="";
  identityNumber="";
  constructor(private _suppluerService:GetSuppliersService, private router:Router, private ref: DynamicDialogRef, public config: DynamicDialogConfig ) { }

  ngOnInit(): void {
    console.log(this.config.data.Supplier , "hguyrytectrxwyrewxhdu")
    this.supplierName = this.config.data.Supplier.SupplierName;
    this.zone = this.config.data.Supplier.Zone;
    this.address = this.config.data.Supplier.Address;
    this.telephone = this.config.data.Supplier.Telephone;
    this.city = this.config.data.Supplier.City;
    this.contactName = this.config.data.Supplier.ContactName;
    this.identityNumber = this.config.data.Supplier.IdentityNumber;
  }
  supplier: Suppliers = new Suppliers();
  newSupplier(){
    this.supplier.SupplierName = this.supplierName;
    this.supplier.Zone = this.zone;
    this.supplier.Address = this.address;
    this.supplier.Telephone = this.telephone.toString();
    this.supplier.IdentityNumber = this.identityNumber.toString();
  //  this.supplier.CityCode = this.city;
    this.supplier.ContactName = this.contactName;
    this._suppluerService.createSupplier(this.supplier).subscribe(res=>{
        if(res.ResponseCode == "0")
        {
          alert("added supplier");
          this.router.navigate(['/suppliers-list']);
          window.location.reload();
    
        }
        else if(res.ResponseCode == "101")  {
          alert("student already exist");
          window.location.reload();
      } 
      else{
        alert("error");
        
      }
    });
   
     }
     updateSupplier(){ 
    
      this.supplier.SupplierId = this.config.data.Supplier.SupplierId;
      console.log("hgfds");
      this.supplier.SupplierName = this.supplierName;
      this.supplier.Address = this.address;
     // this.supplier.CityCode = this.city;
      this.supplier.Telephone = this.telephone;
      this.supplier.ContactName = this.contactName;
      this.supplier.Zone = this.zone;
      this.supplier.IdentityNumber = this.identityNumber;
       this._suppluerService.updateSupplier(this.supplier).subscribe(res => {
        this.ref.close();
        window.location.reload();
       }
       );
      }
      exit(){
        this.ref.close();
      }
    
}
