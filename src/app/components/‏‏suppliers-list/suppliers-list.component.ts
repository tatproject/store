import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DialogService, DynamicDialogRef } from 'primeng-lts/dynamicdialog';
//import { DialogService } from 'primeng-lts/dynamicdialog/dialogservice';
import { Suppliers } from 'src/app/Models/suppliers';
import { GetSuppliersService } from 'src/app/services/‏‏‏‏get-suppliers.service';
import { NewSupplierComponent } from '../new-supplier/new-supplier.component';
 import { Inject } from '@angular/core';
  import { DOCUMENT } from '@angular/common'; 
import { DeletePopUpComponent } from '../popUps/delete-pop-up/delete-pop-up.component';

@Component({
  selector: 'app-suppliers-list',
  templateUrl: './suppliers-list.component.html',
  styleUrls: ['./suppliers-list.component.css']
})
export class SuppliersListComponent implements OnInit {
supplier: Suppliers = new Suppliers();

constructor(private GetsuppliersService:GetSuppliersService,private router:Router, public dialogService: DialogService,@Inject(DOCUMENT) private document: Document) { 
  }
  searchWord="";
  ref: DynamicDialogRef | undefined;
  suppliers: Suppliers[] =[];
  filteredSuppliers: Suppliers[] =[];
 first = 0;

  rows = 10;

  ngOnInit():void {
   this.getsupplierlist();
 
  }  
  route(id:number){
  if(id ==2)
    this.router.navigate(['/new-supplier']);
    else if(id ==3)
    this.editSupplier()}

 
     // this.GetStudentsService.getStudentList().then(students => this.students = students);
  

  next() {
      this.first = this.first + this.rows;
  }
  prev() {
      this.first = this.first - this.rows;
  }
  reset() {
      this.first = 0;
  }
  isLastPage(): boolean {
      return this.suppliers ? this.first === (this.suppliers.length - this.rows): true;
  }
  isFirstPage(): boolean {
      return this.suppliers ? this.first === 0 : true;
  }
  
  getsupplierlist(){
    this.GetsuppliersService.getSupplierList().subscribe(res => { 
      console.log(res);
      this.suppliers=res.supplierModel;
    })}
    

    editSupplier(){
        this.ref = this.dialogService.open(NewSupplierComponent , {
      data:{
      },
      header : 'ספק חדש',
      width : '50%'
    })

    }
  
 
    
      UpdateSupplier(Supplier:Suppliers){
        console.log(Supplier);
        this.ref = this.dialogService.open(NewSupplierComponent , {
          data:{
            Supplier : Supplier
          },
          header : 'ערוך ספק',
          width : '50%'
        }) }
         
   search(){
    console.log(this.searchWord)
         if(!this.searchWord){this.getsupplierlist()}
        else{this.suppliers.forEach(item=>{item.SupplierName?.indexOf(this.searchWord)!== -1?
          this.filteredSuppliers.push(item):
        console.log("jj")})
        this.suppliers=this.filteredSuppliers
        this.filteredSuppliers=[]
      }
   }
   
   deletesupplier(SupplierId:any){
    const dialogRef = this.dialogService.open(DeletePopUpComponent , {
      header : 'שים לב!',
      width : '50%'
    });
    dialogRef.onClose.subscribe(res=>{
      if(res == false)
      { 
        this.GetsuppliersService.deleteSupplier(SupplierId).subscribe(res => { 
        this.router.navigate(['/suppliers-list']).then(() => {
           window.location.reload();});  
        })}

      console.log(res , "response from ref")
    })

  }
      
}
