
import { Router } from '@angular/router';
import { Students } from 'src/app/Models/students';
import { GetStudentsService } from 'src/app/services/‏‏get-students.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {DynamicDialogRef} from 'primeng-lts/dynamicdialog';
import {DynamicDialogConfig} from 'primeng-lts/dynamicdialog'

@Component({
  selector: 'app-new-customer',
  templateUrl: './new-customer.component.html',
  styleUrls: ['./new-customer.component.css']
})
export class NewCustomerComponent implements OnInit {
  id="";
  identityNumber="";
  firstName:string = "";
  lastName = "";
  telephone=" ";
  email="";
  customer:Students = new Students();
  constructor(private _studentsService:GetStudentsService, private router:Router , private ref: DynamicDialogRef, public config: DynamicDialogConfig ) { }

  ngOnInit(): void {
    console.log(this.config.data.Customer , "hguyrytectrxwyrewxhdu")
this.email = this.config.data.Customer.Email;
this.firstName = this.config.data.Customer.FirstName;
this.lastName = this.config.data.Customer.LastName;
this.telephone = this.config.data.Customer.Telephone;
this.identityNumber = this.config.data.Customer.IdentityNumber;
  

// this.customer=this._studentsService.student;
// this.lastName=this.customer.lastName!;
// this.firstName=this.customer.firstName!;
// this.email=this.customer.email!;
// this.identityNumber=this.customer.identityNumber!;
// this.telephone=this.customer.telephone!;

//console.log(this.customer," aaa");
  }
student: Students = new Students();

newCustomer(){
  this.student.identityNumber = this.identityNumber.toString();
  this.student.FirstName = this.firstName;
  this.student.lastName = this.lastName;
  this.student.telephone = this.telephone.toString();
  this.student.email = this.email;
  this._studentsService.createStudent(this.student).subscribe(res=>{
 
    if(res.ResponseCode == "0")
    {
      alert("added student");
      this.router.navigate(['/students-list']);
      window.location.reload();

    }
    else if(res.ResponseCode == "101")  {
      alert("student already exist");
      window.location.reload();
  } 
    else{
      alert("error");
      
    }
  });}
  updateCustomer(){ 
    
    this.student.customerId = this.config.data.Customer.CustomerId;
    console.log("hgfds");
    this.student.identityNumber = this.identityNumber.toString();
    this.student.FirstName = this.firstName;
    this.student.lastName = this.lastName;
    this.student.telephone = this.telephone.toString();
    this.student.email = this.email;
     this._studentsService.updateStudent(this.student).subscribe(res => {
      this.ref.close();
      window.location.reload();
     }
     );
    }
  
  exit(){
    this.ref.close();
  }

   
}
