import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'


@Injectable({
  providedIn: 'root'
})
class student{
   id:number | undefined;
   name:string | undefined

}
export class GetStudentService {

  private apiURL="https://localhost:44327"
httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })}
  
getAll(): Observable<student[]> {
  return this.httpClient.get<student[]>(this.apiURL+'/student/')
  .pipe(
    catchError(this.errorHandler)
  )
  }
  
  
errorHandler(error:any) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    errorMessage = error.error.message;
  } else {
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  return throwError(errorMessage);
}
  constructor(
    private httpClient: HttpClient) { 
    
  }
}

