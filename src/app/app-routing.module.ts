import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { NewCustomerComponent } from './components/new-customer/new-customer.component';
import { DebtsComponent } from './components/debts/debts.component';
import { HomeComponent } from './components/home/home.component';
import { NewSupplierComponent } from './components/new-supplier/new-supplier.component';
import { StudentsListComponent } from './components/students-list/students-list.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { StoreComponent } from './components/store/store.component';
import{SuppliersListComponent}from'./components/‏‏suppliers-list/suppliers-list.component';
import { CategoryComponent } from './components/category/category.component';
import { BasketComponent } from './components/basket/basket.component';

const routes: Routes = [
  {
    path:'login',component:LoginComponent}
   ,{ path:'debts',component:DebtsComponent},
   { path:'debts/:CustomerId',component:DebtsComponent},
  {  path:'home',component:HomeComponent},
  { path:'new-supplier' ,component:NewSupplierComponent},
  {  path:'students-list',component:StudentsListComponent},
  { path:'nav-bar',component:NavBarComponent},
  { path:'store',component:StoreComponent},
  {path: 'categorys' , component: CategoryComponent},
  {path:'store/:id',component:StoreComponent},
    {path:'suppliers-list',component:SuppliersListComponent},
    {path : 'basket' , component:BasketComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }