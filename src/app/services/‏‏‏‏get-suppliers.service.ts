import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Suppliers } from 'src/app/Models/suppliers';

@Injectable({
  providedIn: 'root'
})
export class GetSuppliersService {

  constructor(private http: HttpClient) { }
  public supplier:Suppliers = new Suppliers;
    headers = {'content-type': 'application/json'}
    getSuppliers(){
      let body = {};
      console.log(body , "body")
      return this.http.post('/api/login', body, {headers:this.headers});
    
  }
  getSupplierList(){
    let body = {};
      console.log(body , "body")
      return this.http.post<any>('/api/GetSupplier', body, {headers:this.headers});
 }
 deleteSupplier(SupplierId: string) { 
  let body = {};
  console.log(body , "body") 
 // const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
  return this.http.delete<number>('/api/DeleteSupplier?id=' +SupplierId, body);  }

  createSupplier (supplier: Suppliers): Observable<any> {
    let body = JSON.stringify(supplier)
    console.log(body , "body")
    return this.http.post<any>('/api/AddNewSupplier', body, {headers:this.headers});
}
updateSupplier(supplier:Suppliers){
  let body = JSON.stringify(supplier);
  console.log(body , "body") 
 // const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
  return this.http.post<any>('/api/UpdateSupplier'  , body, {headers:this.headers});  

}
 
}
