import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../Models/products';

@Injectable({
  providedIn: 'root'
})
export class GetProductsService {

  constructor(private http: HttpClient) { }

    headers = {'content-type': 'application/json'}
    getProducts(){
      let body = {};
      return this.http.post('/api/login', body, {headers:this.headers});
    
  }
  getStoreItems(id : string){
    let body = JSON.stringify(id);
      return this.http.post<any>('/api/GetSroreItems', body, {headers:this.headers});
  }
  getBasketItems(basket : string){
    let body = JSON.stringify(basket);
      return this.http.post<any>('/api/GetBasketItems', body, {headers:this.headers});
  }
  RemoveBasketItems(basket : string){
    let body = JSON.stringify(basket);
      return this.http.post<any>('/api/RemoveQuantity', body, {headers:this.headers});
  }

  getCategorys(){
    let body = {};
      return this.http.post<any>('/api/GetCategorys', body, {headers:this.headers});

  }
  UpdateProduct(product: Product){
    let body = JSON.stringify(product);
    return this.http.post('/api/UpdateProduct', body, {headers:this.headers});
  
}
   DeleteProduct(id : string){
    let body = JSON.stringify(id);
      return this.http.post<any>('/api/DeleteProduct', body, {headers:this.headers});
  }
  NewProduct(product: Product){
    let body = JSON.stringify(product);
    return this.http.post('/api/SetNewProduct', body, {headers:this.headers});
  
}
}
