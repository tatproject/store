import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginModel } from '../Models/LoginModel';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

    headers = {'content-type': 'application/json'}
    login(login: LoginModel): Observable<any> {
      let body = JSON.stringify(login)
      console.log(body , "body")
      return this.http.post<any>('/api/login', body, {headers:this.headers});
    
  }
}
