import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Students } from 'src/app/Models/students';
import { Debts } from '../Models/DebtsModel';


@Injectable({
  providedIn: 'root'
})
export class GetStudentsService {

  constructor(private http: HttpClient) { }
public student:Students = new Students;
public debts:Debts[]=[];
    headers = {'content-type': 'application/json'}
    getStudents(){
      let body = {};
      console.log(body , "body")
      return this.http.post('/api/login', body, {headers:this.headers});
    
  }
  getStudentList(){
    let body = {};
      console.log(body , "body")
      return this.http.post<any>('/api/GetStudentList', body, {headers:this.headers});

  }
  deleteStudent(customerid: string) { 
    let body = {};
    console.log(body , "body") 
   // const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.delete<number>('/api/DeleteStudent?id=' +customerid, body);  
  } 
  createStudent (student: Students): Observable<any> {
      let body = JSON.stringify(student)
      console.log(body , "body")
      return this.http.post<any>('/api/CreateStudent', body, {headers:this.headers});
  }
  updateStudent(customer:Students){
    let body = JSON.stringify(customer)
   // const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.post<any>('/api/UpdateStudent' , body, {headers:this.headers});  

  }
  AddDebtsToStudend(customer:Debts){
    let body = JSON.stringify(customer)
   // const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.post<any>('/api/AddDebtsToStudend' , body, {headers:this.headers});  

  }
  GetDebts(customerId:string){
    let body = {};
    console.log(body , "body") 
   // const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.post<any>('/api/GetDebts?id=' +customerId, body);  

  }
  Search(firstName:string){
    let body = {};
    console.log(body , "body")
    return this.http.post<any>('/api/Search?firstName='+firstName, body);

  }
    
    

}
